# Padrão Digital de Governo - Site

## Informações

Esse projeto é feito usando Angular 8+.

Esse repositório não é o Design System. Caso esteja procurando o Design System procure no mesmo grupo o repositório correspondente.

O Design System é consumido como submódulo git na pasta assets. Assim, sempre que for necessário alguma versão específica do Design System é necessário atualizar o submódulo.

## Tecnologias

Esse projeto é desenvolvido usando:

1. [Angular](https://angular.io/ 'Angular').
1. [SASS](https://sass-lang.com/ 'SASS')

## Dependências

As principais dependências do projeto são:

1. [Design System GOV.BR.BR](https://cdngovbr-ds.estaleiro.serpro.gov.br/ 'Design System')

1. [Font Awesome](https://fontawesome.com/ 'Font Awesome')

1. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ 'Fonte Rawline')

> O fontawesome e a fonte rawline podem ser importadas de um CDN. Consulte a documentação no site do [Design System](http://cdngovbr-ds.estaleiro.serpro.gov.br/ 'Design System') para mais detalhes.

## Como executar o projeto?

Esse exemplo vai rodar o site com conteudo vindo de docs-ds.estaleiro.serpro.gov.br

```sh
git clone git@gitlab.com:govbr-ds/tools/site/govbr-ds-site-angular.git

git checkout main

npm install

npm run init

npm run start
```

> Se o seu sistema operacional for windows, habilite os caminhos longos para arquivos: [https://helpdeskgeek.com/how-to/how-to-fix-filename-is-too-long-issue-in-windows/](https://helpdeskgeek.com/how-to/how-to-fix-filename-is-too-long-issue-in-windows/)

### Rodando o site de desenvolvimento com conteudo local

Deve rodar essa sequencia de comandos depois do clone do repositório

```bash
npm install

npm run init

npm run dev:all
```

### Atualizando conteudo de componentes localmente

Os conteúdos dos componentes e documentos do site são submódulos desse repositório. e deve utilizar o comando abaixo para fazer o clone ou rodar **git submodule update --init --recursive**

```bash
npm run init
```

Os repositórios estão localizados dentro da pasta 'content', e ao efetuar mudanças neles, é necessário criar uma solicitação de merge nos próprios repositórios, direcionada para a branch principal. Para testar localmente, execute o comando **npm run dev:all**

#### Core

É o padrão em que os exemplos são compilados que fica na pasta **conteudo/govbr-ds-core**.
É preciso rodar o build para que os exemplos sejam mostrados usando o comando abaixo

```bash
npm run build
```

#### Documentos

Abaixo a pasta aonde se encontra os documentos de Design System

> **/content/dsgov-br**

Abaixo a pasta onde estão os documentos do site que não são modificados por versão do Design System.

> **/contetn/dsgov-br-site-content**

### Rodando o site com conteudo atualizado com conteudo externo

```bash
npm run start
```

## Resolvendo problemas com o submódulo

<details>
  <summary>Problema para baixar o submódulo</summary>

```bash
git config --global http.sslverify false
```

</details>

<details>
  <summary>Submódulo não atualiza ou pasta está vazia.
  </summary>

> Atenção esse comando abaixo troca as branchs para main

```bash
git submodule update --init --recursive
```

</details>

##  Lints

Nesse projeto usamos diversos tipos de lints para automaticamente verificar o código antes de enviar para review. Para executá-los e obter os resultados execute o comando:

```bash
npm run lint
```

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do Design System GovBR-DS <http://gov.br/ds/>

- Usando nosso canal no discord <https://discord.gg/NkaVZERAT7>

## Como contribuir?

Por favor verifique nossos guias de [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Créditos

Os Web Components do Design System são criados pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.

This project is tested with [BrowserStack](https://www.browserstack.com/).
