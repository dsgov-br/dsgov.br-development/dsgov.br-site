/**
 * Script para gerar o JSON no formato correto para o Algolia
 * Ao final gera um arquivo algolia.json na raiz da página
 */
import fs from 'fs'
import { globby } from 'globby'
import matter from 'gray-matter'

let algoliaIndex = []

// Busca o arquivo config.json e transforma em json
let config = JSON.parse(fs.readFileSync('src/assets/config.json', 'utf8'))

searchForFilesMetadata()

/**
 * Itera no config.json e chama o método correspondende ao tipo do dado
 */
async function searchForFilesMetadata() {
	for (const child of config.children) {
		const pageUrl = `content/govbr-ds-core/dist/${child.id}/**/*.{md,html}`
		const pageSite = `content/**/*.{md,html}`
		const pages = await globby([
			pageUrl,
			pageSite,
			'!content/govbr-ds-site-content/node_modules/**/*.*',
			'!content/govbr-ds/node_modules/**/*.*',
			'!content/govbr-ds-core/node_modules/**/*.*',
			,
			'!content/govbr-ds-core/dist/**/*.*',
			'!content/govbr-ds-core/docs',
			'!content/govbr-ds-core/dist/**/examples/**/*.*',
			'!content/govbr-ds-core/dist/report.html',
			`!content/govbr-ds-core/dist/util/cores/examples.html`,
			'!content/govbr-ds-site-core/node_modules/**/*.*',
		])
		convertDocsFolderToJson(pages)
		// ---

		// Página de downloads
		if (child.id === 'assets') convertDownloadsToJson(child)

		// Manuais
		if (child.id === 'guias') convertManuaisToJson(child)

		// Legislação
		if (child.id === 'legislacao') convertLegislacaoToJson(child)
	}

	const allDocsAsString = JSON.stringify(algoliaIndex)

	fs.writeFile('algolia.json', allDocsAsString, 'utf8', function (err) {
		if (err) {
			console.error('Não foi possível converter as páginas.')
			return console.log(err)
		}

		console.info('Aquivo JSON criado na raiz do projeto!')
	})
}

function convertDocsFolderToJson(pages) {
	const jsonPages = pages
		.map((page) => {
			const fileContents = fs.readFileSync(page, 'utf8')
			let { data, content } = matter(fileContents)

			const reg = '/(?!.*/)'
			const splitPath = page.split(new RegExp(reg, 'gm'))
			let [slug, filename] = splitPath

			slug = slug.replace('src/assets/govbr-ds-dev-core/docs/', '')
			slug = slug.replace('content/govbr-ds/', '')
			slug = slug.replace('content/govbr-ds-site-content/', '')
			slug = slug.replace('content/govbr-ds-core/', '')
			slug = slug.replace('ds/', '')

			// Varre config.json a procura do label
			const splitedSlug = slug.split('/')
			let pageConfig = JSON.parse(JSON.stringify(config.children))
			for (const slug of splitedSlug) {
				if (Array.isArray(pageConfig)) {
					pageConfig.forEach((page) => {
						if (page.id === slug) {
							if (page.children) {
								pageConfig = page.children
							} else {
								pageConfig = page
							}
						}
					})
				}
			}

			// URl é o slug + abas, caso existam

			if (filename.includes('-dev')) {
				slug += '?tab=desenvolvedor'

				pageConfig.label = `${pageConfig.label} (Desenvolvedor)`
			}
			if (filename.includes('-access')) {
				slug += '?tab=acessibilidade'

				let nameFile = filename.split('-')

				if (pageConfig.label) {
					pageConfig.label = `${pageConfig.label} (Acessibilidade)`
				} else {
					pageConfig.label = `${nameFile[0]} (Acessibilidade)`
				}
			}

			if (page.includes('componentes/') && !filename.includes('-access')) {
				slug += '?tab=designer'
				let nameFile = filename.split('.')
				if (pageConfig.label) {
					pageConfig.label = `${pageConfig.label} (Designer)`
				} else {
					pageConfig.label = `${nameFile[0]} (Designer)`
				}
			}

			slug = slug.replace('componentes/', 'components/')
			let url = slug

			if (content.length < 5) {
				return {}
			}
			return {
				slug,
				filename,
				url,
				label: pageConfig.label,
				content,
				frontmatter: {
					...data,
				},
			}
		})
		.filter(Boolean)
	algoliaIndex.push(...jsonPages)
}

function convertDownloadsToJson(child) {
	let downloads = []
	for (const section of child.sections) {
		for (const file of section.files) {
			downloads.push({
				label: file.label,
				description: file.description,
				image: file.image,
				arquivos: file.children,
				slug: 'downloads/assets',
				url: 'downloads/assets',
			})
		}
	}
	algoliaIndex.push(...downloads)
}

function convertManuaisToJson(child) {
	let manuais = []
	for (const section of child.children) {
		if (section.id === 'manuais-orientadores') {
			for (const file of section.files) {
				manuais.push({
					description: file.description,
					label: file.label,
					image: file.image,
					slug: 'downloads/manuais-orientadores',
					url: 'downloads/manuais-orientadores',
				})
			}
		}
	}
	algoliaIndex.push(...manuais)
}

function convertLegislacaoToJson(child) {
	let legislacao = []
	for (const file of child.files) {
		legislacao.push({
			description: file.description,
			label: file.label,
			image: file.image,
			slug: 'downloads/legislacao',
			url: 'downloads/legislacao',
		})
	}
	algoliaIndex.push(...legislacao)
}
