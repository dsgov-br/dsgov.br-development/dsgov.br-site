import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { IDefaultConfig } from '../interfaces/defaultConfig'
import { ISelectedFile } from '../interfaces/selectedFile'

/** Serviço para guardar as configurações compartilhadas entre componentes
 * As propriedades podem ser assinadas (subscribe)
 */
@Injectable({
	providedIn: 'root',
})
export class StorageService {
	// Arquivo de configuração completo
	private config = new BehaviorSubject<{}>([])
	currentConfig = this.config.asObservable()

	// Configurações default dos tipos
	defaultConfig: IDefaultConfig[]

	// Arquivo selecionado
	private selectedFile = new BehaviorSubject<ISelectedFile>(null)
	currentSelectedFile = this.selectedFile.asObservable()

	// Estado do menu (visível ou não)
	private showMenu = new BehaviorSubject<boolean>(this.menuInitState())
	currentState = this.showMenu.asObservable()

	toogleMenu() {
		this.showMenu.next(!this.showMenu.value)
	}

	setShowMenu(value: boolean) {
		this.showMenu.next(value)
	}

	setConfig(data: {}) {
		this.defaultConfig = data['defaultTypeConfig']
		this.config.next(data)
	}

	setCurrentSelectedFile(data: ISelectedFile) {
		if (!data) return null

		const [typeConfig] = this.defaultConfig
			.filter((config) => {
				return config.forType.includes(data.type)
			})
			// Remove o forType
			.map(({ forType, ...config }) => config)

		// Merge defaultConfig sobrescrevendo com o que estiver no data
		data = { ...typeConfig, ...data }
		this.selectedFile.next(data)
	}

	/** Busca recursiva para identificar objeto usando o par key/value
	 * @param obj Objeto da busca
	 * @param prop Propriedade usada para comparação
	 * @param value Valor a buscar
	 */

	findSelectedFile = (obj: {}, prop: string, value: string) => {
		if (obj[prop] === value) {
			return obj
		}

		for (let i = 0, len = Object.keys(obj).length; i < len; i++) {
			if (typeof obj[i] === 'object' && obj[i] !== null) {
				let childObject: {}
				if (obj[i].children) childObject = obj[i].children
				if (!obj[i].children) childObject = obj[i]

				const found = this.findSelectedFile(childObject, prop, value)
				if (found) {
					return found
				}
			}
		}
	}

	/**
	 * Retorna um array de objetos de acordo com o par chave/valor
	 */
	findObject(obj: {}, key: string, val: string) {
		for (const i in obj) {
			if (!obj.hasOwnProperty(i)) continue

			if (typeof obj[i] == 'object') {
				const result = this.findObject(obj[i], key, val)
				if (result && Object.keys(result).length !== 0) return result
			}

			if (i == key && obj[i] == val) return { ...obj }
		}
	}

	/**
	 *
	 * @returns se o menu deve iniciar aberto
	 */
	menuInitState() {
		if (window.innerWidth < 575) {
			return false
		}
		return true
	}
}
