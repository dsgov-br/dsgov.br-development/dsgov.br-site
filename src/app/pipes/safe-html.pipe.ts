import { Pipe, PipeTransform } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
/*
 * Marca HTML como seguro para renderização pelo angular
 */
@Pipe({ name: 'safeHtml' })
export class SafeHtmlPipe implements PipeTransform {
	constructor(private sanitizer: DomSanitizer) {}

	transform(value) {
		return this.sanitizer.bypassSecurityTrustHtml(value)
	}
}
