import { Location } from '@angular/common'
import {
	AfterViewInit,
	Component,
	ElementRef,
	HostListener,
	OnDestroy,
	OnInit,
	QueryList,
	ViewChildren
} from '@angular/core'
// import Menu from 'src/assets/govbr-ds-dev-core/src/components/menu/menu.js'
import { BRMenu } from '@govbr-ds/core/dist/core'
import { ISelectedFile } from '../interfaces/selectedFile'
import { StorageService } from '../services/storage.service'

@Component({
	selector: 'br-menu',
	styleUrls: ['./menu.component.scss'],
	templateUrl: './menu.component.html',
})
export class MenuComponent implements OnInit, AfterViewInit, OnDestroy {
	@ViewChildren('brMenu') brMenu: QueryList<ElementRef>
	@ViewChildren('dropMenu') dropMenu: QueryList<ElementRef>

	config: any
	menu: any
	showMenu: boolean
	selectedFile: ISelectedFile
	menuInstance: any = null
	thrirdLevelMenu
	currentURL: string
	event$

	constructor(private storageService: StorageService, private location: Location) {
		this.event$ = location.onUrlChange((val) => {
			this.currentURL = val
			this.synchronizeMenuURL()
		})
	}

	ngOnInit() {
		this.storageService.currentConfig.subscribe((config) => {
			this.config = config
		})

		this.storageService.currentSelectedFile.subscribe((selectedFile: ISelectedFile) => {
			if (selectedFile) {
				this.selectedFile = selectedFile
			}
		})

		this.storageService.currentState.subscribe((showMenu) => {
			this.showMenu = showMenu
			this.setBodyOverflow()
		})
	}

	ngOnDestroy() {
		this.event$.unsubscribe()
	}

	ngAfterViewInit() {
		this.menu = this.brMenu.toArray()[0].nativeElement
		this.setMenu()
		if (window.matchMedia('(min-width: 575px').matches) {
			this.menuInstance._openMenu()
		}
		this.setMenuState()
		this.setKeyboardBehaviour()
	}

	setKeyboardBehaviour() {
		this.menu.addEventListener('keyup', (event) => {
			switch (event.code) {
				case 'Escape':
					this.storageService.setShowMenu(false)
				default:
					break
			}
		})
	}

	/**
	 * Cria o routerlink dinamicamente dependendo do tipo do item
	 */
	setRouterLink(item: ISelectedFile, parent?: string) {
		// Itens de categoria
		if (parent && item.type === 'downloads') return [item.type, item.id]
		if (parent) return [parent, item.id]

		// Itens sem categoria
		if (item.type === 'documentation') return [item.id]
		if (item.type === 'downloads') return [item.type, item.id]
		return '/'
	}

	setMenuState() {
		if (window.innerWidth < 575) {
			this.storageService.setShowMenu(false)
			this.menu.classList.remove('active')
		} else {
			this.storageService.setShowMenu(true)
			this.menu.classList.add('active')
		}
	}

	setMenu() {
		if (!this.menuInstance) {
			this.menuInstance = new BRMenu('br-menu', this.menu)
		}
	}

	/**
	 * Sincroniza a rota com a parte do menu que dá acesso a essa rota.
	 */
	synchronizeMenuURL() {
		let section = null
		const pathSections = this.currentURL.split('/')

		if (pathSections.length > 3) {
			switch (pathSections[2]) {
				/** A rota de manuais orientadores precisa do DownloadComponent.
				 * Porém no menu ela está na pasta guias.
				 * Foi feito um tratamento de exceção para essa página.
				 */
				case 'downloads':
					switch (pathSections[3]) {
						case 'manuais-orientadores':
							section = this.menu.querySelector('#guias')
							if (!section.parentNode.classList.contains('active')) {
								section.click()
							}
							break
					}
					break
				/**
				 * Caso padrão para as demais rotas
				 */
				default:
					section = this.menu.querySelector(`#${pathSections[pathSections.length - 2]}`)
					if (!section.parentNode.classList.contains('active')) {
						section.click()
					}
			}
		} else if (pathSections.length === 3) {
			switch (pathSections[2]) {
				case 'home':
					section = this.menu.querySelector('li.side-menu.active a')
					if (section) {
						section.click()
					}
					break
			}
		}
		if (window.matchMedia('(max-width: 575px)').matches) {
			this.storageService.setShowMenu(false)
			this.menuInstance._closeMenu()
		}
	}

	closeOthersFolders(event) {
		this.menu.querySelectorAll('.menu-folder').forEach((menuFolder) => {
			if (menuFolder !== event.currentTarget) {
				menuFolder.classList.remove('active')
			}
		})
	}

	/**
	 * Monitora o evento de resize do browser para alterar o estado do menu e body
	 */
	@HostListener('window:resize', [])
	onResize() {
		this.setMenuState()
		this.setBodyOverflow()
	}

	/**
	 * Retira o scroll do body no menu aberto no mobile pois o menu ocupa a tela toda
	 */
	private setBodyOverflow() {
		if (this.showMenu && window.innerWidth < 575) document.body.style.overflow = 'hidden'

		if (!this.showMenu || window.innerWidth >= 575) document.body.style.overflow = 'auto'
	}
}
