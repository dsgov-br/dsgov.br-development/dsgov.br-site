import { Component, OnInit } from '@angular/core'
import { DomSanitizer, SafeHtml } from '@angular/platform-browser'
import { environment } from 'src/environments/environment'
import { ISelectedFile } from '../interfaces/selectedFile'
import { ITab } from '../interfaces/tab'
import { FilesService } from '../services/files.service'
import { NavigationService } from '../services/navigation.service'
import { StorageService } from '../services/storage.service'
import { UtilService } from '../services/util.service'

declare const Prism: any

@Component({
	selector: 'br-content',
	styleUrls: ['./content.component.scss'],
	templateUrl: './content.component.html',
})
export class ContentComponent implements OnInit {
	selectedFile: ISelectedFile
	docDefault: SafeHtml
	isMarkdown: boolean
	tabs: ITab[]
	messageTimeout: any
	expandButton: any
	docNoTitle: SafeHtml

	constructor(
		private storageService: StorageService,
		private fileService: FilesService,
		private utilService: UtilService,
		private sanitizer: DomSanitizer,
		private navigationService: NavigationService
	) {}

	ngOnInit(): void {
		this.storageService.currentSelectedFile.subscribe((selectedFile: ISelectedFile) => {
			// Limpa o conteúdo sempre que mudar de rota
			this.resetContent()
			
			if (selectedFile) this.selectedFile = { ...selectedFile }
					

			if (this.selectedFile?.ext) {
				this.setContent()
			}
		})

		this.addCopyCodeBtn()
		this.addExpandCodeBtn()
	}

	ngAfterViewInit(): void {
		this.scrollToAnchor()
	}

	resetContent() {
		this.docDefault = ''

		this.tabs = [
			{
				content: null,
				id: 1,
				isActive: true,
				label: 'Desenvolvedor',
				showOnUrl: true,
			},
			{
				content: null,
				id: 2,
				label: 'Designer',
				showOnUrl: true,
			},
			{
				content: null,
				id: 3,
				label: 'Acessibilidade',
				showOnUrl: true,
			},
		]
	}

	/**
	 * Monta as URLs e busca os recursos
	 */
	setContent() {
		// Se for markdown usa o ngx-markdown para renderizar a página
		this.isMarkdown = this.selectedFile.ext.includes('md')
		let extension: string

		// Se o ext do config tiver MD dá preferência ao markdown
		if (this.isMarkdown) extension = 'md'
		if (!this.isMarkdown && this.selectedFile.ext.includes('html')) extension = 'html'

		const url = this.createUrl(extension)
		
		// let environmentDoc = `${environment.designDocUrl}`
		if(this.selectedFile.repository === 'core'){
			this.fetchDefaultData(this.createUrldesign(extension, `${environment.designDocUrl}`))
		}else{
			this.fetchDefaultData(this.createUrldesign(extension, `${environment.staticContentUrl}`))
		}
		this.fetchDeveloperData(url)
		this.fetchDesignerData(this.createUrldesign(extension, `${environment.designDocUrl}`))
		this.fetchAccessibilityData(this.createUrldesign(extension, `${environment.designDocUrl}`))
		

	}

	createUrldesign(extension: string, url: string) {
		if (this.selectedFile.path) url = `${this.selectedFile.path}`

		if (!this.selectedFile.path) {
			this.selectedFile.routes.forEach((route, index) => {
				if (route) {
					url += `${route}`
					const isLast = index === this.selectedFile.routes.length - 1
					if (isLast) url += `/${route}.${extension}`
					if (!isLast) url += '/'
				}
			})
		}
		return url
	}

	createUrl(extension: string) {
		let url = `${environment.docUrl}`

		if (this.selectedFile.path) url = `${this.selectedFile.path}`

		if (!this.selectedFile.path) {
			this.selectedFile.routes.forEach((route, index) => {
				if (route) {
					url += `${route}`
					const isLast = index === this.selectedFile.routes.length - 1
					if (isLast) url += `/${route}.${extension}`
					if (!isLast) url += '/'
				}
			})
		}
		return url
	}

	fetchDefaultData(url: string) {
		if (this.selectedFile.default) {
			this.fileService.fetchData(url, 'text').subscribe((data) => {
				this.docDefault = this.treatData(String(data), url)
				this.docNoTitle = this.treatData(this.removeLineHead(String(data)), url)
				this.expandButton = false
			})
		}
	}

	fetchAccessibilityData(url: string) {
		if (this.selectedFile.accessibility) {
			let urlAccessibility = this.replaceUrls(url, 'access')
			urlAccessibility = urlAccessibility.replace('components', 'componentes')
						this.fileService.fetchData(urlAccessibility, 'text').subscribe(
				(data) => {
					return this.tabs.map((tab) => {
						if (tab.id === 3) tab.content = this.treatData(String(data), url)
					})
				},
				() => {
					return this.removeBlankTabs(3)
				}
			)
		} else {
			this.removeBlankTabs(3)
		}
	}

	fetchDesignerData(url: string) {
		if (this.selectedFile.designer) {
			const urlDesigner = this.replaceUrls(url, 'dsg')
			// content/govbr-ds/ds/componentes/datetimepicker/datetimepicker.md
			let urlDesignerDiretriz = url.replace('components', 'componentes')
			// urlDesignerDiretriz = url.replace('template', 'padroes/designs')

			if (urlDesignerDiretriz.includes('templates')) {
				urlDesignerDiretriz = url.replace('templates', 'padroes/design')
			}
			this.fileService.fetchData(urlDesignerDiretriz, 'text').subscribe(
				(data) => {
					return this.tabs.map((tab) => {
						let datas = this.removeLineHeadDesigner(String(data))
						if (tab.id === 2) tab.content = this.treatData(datas, urlDesignerDiretriz)
					})
				},
				() => {
					return this.removeBlankTabs(2)
				}
			)
		} else {
			this.removeBlankTabs(2)
		}
	}

	removeLineHeadDesigner(data: string) {
		let lines = data.split('\n')
		lines.splice(0, 3)
		lines = ['\n <br>'].concat(lines)
		return lines.join('\n')
	}

	removeLineHead(data: string) {
		let lines = data.split('\n')
		lines.splice(0, 2)

		return lines.join('\n')
	}

	fetchDeveloperData(url: string) {
		if (this.selectedFile.developer) {
			const urlDeveloper = this.replaceUrls(url, 'dev')

			this.fileService.fetchData(urlDeveloper, 'text').subscribe(
				(data) => {
					return this.tabs.map((tab) => {
						if (tab.id === 1) tab.content = this.treatData(String(data), url)
					})
				},
				() => {
					return this.removeBlankTabs(1)
				}
			)
		} else {
			this.removeBlankTabs(1)
		}
	}

	/**
	 * Fazer o tratamento correto do conteúdo dependendo do tipo (HTML | MD)
	 * @param data Conteúdo do arquivo
	 * @param url URL usada para buscar o arquivo
	 */
	treatData = (data: string, url: string) => {
		if (this.isMarkdown) {
			return this.fileService.fixUrls(data, url)
		}
		if (!this.isMarkdown) {
			return this.sanitizer.bypassSecurityTrustHtml(this.fileService.fixUrls(data, url))
		}
	}

	/**
	 * Inclui os prefixos nas URLs para a busca dos conteúdos
	 */
	replaceUrls(url: string, suffix: string) {
		return this.fileService.replaceUrls(url, '(.*)(.md|.html)', `$1-${suffix}$2`)
	}

	changeToolbarText(env: any, find: string, replace: string) {
		const toolbarElems = [...env.element.parentElement.nextSibling.childNodes]
		toolbarElems.map((elem) => {
			if (elem.textContent === find) {
				elem.children[0].textContent = replace
			}
		})
	}

	/**
	 * Remover abas em branco
	 */
	removeBlankTabs(id: number) {
		this.tabs = this.tabs.filter((tab) => {
			return tab.id !== id
		})
	}

	addCopyCodeBtn() {
		Prism.plugins.toolbar.registerButton('copy-code', (env: any) => {
			const button = document.createElement('button')
			button.classList.add('br-button', 'small')
			button.innerHTML = 'Copiar'
			button.addEventListener('click', () => {
				const defaultText = 'Copiar'
				const changedText = 'Conteúdo copiado com sucesso!'

				clearTimeout(this.messageTimeout)
				this.utilService.copyText(env.code)
				this.changeToolbarText(env, defaultText, changedText)

				this.messageTimeout = setTimeout(() => {
					this.changeToolbarText(env, changedText, defaultText)
				}, 3000)
			})

			return button
		})
	}

	addExpandCodeBtn() {
		Prism.plugins.toolbar.registerButton('expand-code', (env: any) => {
			if (env.code.length > 1000) {
				const button = document.createElement('button')
				button.classList.add('br-button', 'small')
				button.innerHTML = 'Expandir'

				this.expandButton = true

				button.addEventListener('click', () => {
					const defaultText = 'Expandir'
					const changedText = 'Retrair'

					env.element.parentElement.classList.toggle('expanded')

					if (env.element.parentElement.classList.contains('expanded'))
						this.changeToolbarText(env, defaultText, changedText)

					if (!env.element.parentElement.classList.contains('expanded'))
						this.changeToolbarText(env, changedText, defaultText)
				})
				return button
			}
			return document.createElement('div')
		})
	}

	scrollToAnchor() {
		this.navigationService.scrollToAnchor()
	}
}
