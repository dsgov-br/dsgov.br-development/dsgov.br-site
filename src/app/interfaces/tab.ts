import { SafeHtml, SafeResourceUrl } from '@angular/platform-browser'

export interface ITab {
	content?: SafeHtml
	ext?: string
	id: number
	isActive?: boolean
	isPreview?: boolean
	label: string
	lang?: string
	showOnUrl?: Boolean
	url?: SafeResourceUrl
}
