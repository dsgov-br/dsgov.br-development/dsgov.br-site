import { IFile } from './files'
import { IPreviewTab } from './previewTab'

export interface ISelectedFile {
	id: string
	label: string
	type: string
	ext: string[]
	routes: string[]
	path?: string
	examples?: IPreviewTab[]
	htmlFilename?: string
	hideLabel?: boolean
	default?: boolean
	image?: string
	developer?: boolean
	designer?: boolean
	accessibility?: boolean
	content?: string
	description?: string
	repository?: string
	previewHeight?: string
	children?: ISelectedFile[]
	// Arquivos para download sem categoria
	files?: IFile[]
	// Arquivos para download com categoria
	sections?: IFile[]
}
