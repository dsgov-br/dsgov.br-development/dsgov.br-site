export interface IFile {
	id: string
	path: string
	label: string
	description?: string
	files?: IFile
}
