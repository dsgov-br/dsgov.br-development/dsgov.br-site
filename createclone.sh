
cd ..
git clone git@gitlab.com:govbr-ds/ds/dev/govbr-ds-dev-core.git
git clone git@gitlab.com:govbr-ds/ds/design/govbr-ds-design-diretrizes.git
git clone git@gitlab.com:govbr-ds/ds/dev/govbr-ds-dev-diretrizes.git
git clone git@gitlab.com:govbr-ds/tools/site/govbr-ds-site-content.git
git clone git@gitlab.com:govbr-ds/ds/govbr-ds-writing.git
git clone git@gitlab.com:govbr-ds/ds/govbr-ds-acessibilidade.git
cd govbr-ds-dev-site
npm install
