import { Component, HostListener, OnInit } from '@angular/core'
import { Title } from '@angular/platform-browser'
import { NavigationEnd, Router, RouterEvent } from '@angular/router'
import { GoogleTagManagerService } from 'angular-google-tag-manager'
import { filter } from 'rxjs/operators'
import { ISelectedFile } from './interfaces/selectedFile'
import { FilesService } from './services/files.service'
import { NavigationService } from './services/navigation.service'
import { StorageService } from './services/storage.service'

@Component({
	selector: 'br-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	showMenu: boolean
	config: any
	selectedFile: ISelectedFile
	notFound: boolean

	@HostListener('document:click', ['$event'])
	onDocumentClick(event: Event) {
		this.navigationService.interceptClick(event)
	}

	constructor(
		private fileService: FilesService,
		private router: Router,
		private gtmService: GoogleTagManagerService,
		private storageService: StorageService,
		private navigationService: NavigationService,
		private titleService: Title
	) {}

	ngOnInit() {
		this.storageService.currentState.subscribe((showMenu) => {
			this.showMenu = showMenu
		})

		this.storageService.currentConfig.subscribe((config) => {
			this.config = config
		})

		this.fileService.fetchData(`/ds/assets/config.json`).subscribe((config) => {
			this.storageService.setConfig(config)

			// Ancoras setam "navigated" como FALSE para evitar refresh infinito
			// Rota inicial

			this._setSelectedFile(this.router.url)

			// Outras rotas
			this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe((event: RouterEvent) => {
				// Se tiver o hash e não for a primeira rota do site então não precisa recarregar o conteúdo
				if (event['url'].split('#').filter(Boolean).length === 1) {
					// Navega para o top ao navegar
					window.scrollTo(0, 0)

					this._setSelectedFile(event['url'])
				}

				// push GTM data layer for every visited page
				this.getPageEvents(event)
			})
		})
	}

	getPageEvents(event: RouterEvent) {
		const gtmTag = {
			event: 'page',
			pageName: event.url,
		}

		this.gtmService.pushTag(gtmTag)
	}

	/**
	 * Exemplo de como pegar eventos customizados
	 */
	getCustomEvent() {
		// push GTM data layer with a custom event
		const gtmTag = {
			event: 'button-click',
			data: 'my-custom-event',
		}
		this.gtmService.pushTag(gtmTag)
	}

	_setSelectedFile(url: string) {
		// Algumas rotas, como ds/ e downloads/ (ver app-routing.module.ts) precisam ser ignoradas pois são definidas no arquivo de rotas e não fazem parte do config.json
		const routesToIgnore = ['downloads']

		// Retira rotas em branco e rotas ignoradas
		const routes = url
			.split('/')
			.filter(Boolean)
			.filter((str) => !routesToIgnore.includes(str))
			// Retira ancora e queryParams da última rota para buscar no config.json
			.map((route) => this.navigationService.stripFragmentAndQueryParams(route))

		// Passa por cada rota e busca o object correspondente a essa rota
		// O loop é para garantir que a busca vai ser feita no resultado da anterior
		// Ajudando a encontrar o objeto certo mesmo se existirem IDs repetidos
		let selectedFile = this.config.children

		// Itens herdam id do pai como tipo caso não declarem o seu tipo
		let type: string

		for (const route in routes) {
			selectedFile = this.storageService.findObject(selectedFile, 'id', routes[route])

			if (selectedFile && !type) type = selectedFile.id
			if (selectedFile && !selectedFile.type) selectedFile.type = type
		}

		if (selectedFile) selectedFile.routes = routes

		/** Caso a seção correspondente não seja encontrada no config.json a rota não deve existir */
		this.notFound = !selectedFile

		this.storageService.setCurrentSelectedFile(selectedFile)
		if (selectedFile) this.setTitle(selectedFile, routes)
	}

	setTitle(selectedFile: ISelectedFile, routes: string[]) {
		let url = `Design System `

		// A rota tem uma categoria
		if (routes[0] !== selectedFile.id) {
			const parentRoute = this.storageService.findObject(this.config.children, 'id', routes[0])
			url += ` - ${parentRoute.label}`
		}
		url += ` - ${selectedFile.label}`

		this.titleService.setTitle(url)
	}
}
